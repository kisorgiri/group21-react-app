import React from 'react';
import AppRoutes from './appRouting';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';


// functional component
// components
// basic building block of react
// two ways of writing a component 
// function based ==> stateless  (hooks implementation from react > 16)
// class based ==> statefull

// state is application data (components data)


// componets is used similar to html elements (own elements)



const App = function () {
    return <div className="container">
        <ToastContainer></ToastContainer>
        <AppRoutes></AppRoutes>
    </div>
}

export default App;