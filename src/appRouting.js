import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { Header } from './components/common/header/header.componet';
import { NotFound } from './components/common/notFound/notfound.component';
import { DashboardComponent } from './components/users/dashboard/dashboard.componet';
import { Sidebar } from './components/common/sidebar/sidebar.component';
import ProductForm from './components/products/productForm/productForm';
import ViewProductComponent from './components/products/view-products/view-product.component';
import EditProductComponent from './components/products/edit-product/edit-product.component';
import AddProductComponent from './components/products/add-product/add.product.component';
import { SearchProductComponent } from './components/products/search-product/search-product.component';


const ProtectedRoute = ({ component: Component, ...props }) => (
    <Route {...props} render={(props) => (
        localStorage.getItem('token')
            ? <>
                <div className="nav_bar">
                    <Header isLoggedIn={true}></Header>
                </div>
                <div className="sidenav">
                    <Sidebar></Sidebar>
                </div>
                <div className="main">
                    <Component {...props}></Component>
                </div>
            </>
            : <Redirect to="/"></Redirect> // TODO redirect in better way
    )}></Route>
)

const PublicRoute = ({ component: Component, ...props }) => (
    <Route {...props} render={(props) => (
        <>
            <div className="nav_bar">
                <Header isLoggedIn={localStorage.getItem('token')}></Header>
            </div>
            <div className="main">
                <Component {...props}></Component>
            </div>
        </>
    )}></Route>
)


const AppRoutes = () => {
    return (
        <Router>
            <Switch>
                <PublicRoute exact path="/" component={LoginComponent}></PublicRoute>
                <PublicRoute path="/register" component={RegisterComponent}></PublicRoute>
                <ProtectedRoute path="/view-products" component={ViewProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/add-product" component={AddProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={DashboardComponent}></ProtectedRoute>
                <ProtectedRoute path="/edit-product/:id" component={EditProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/search-product" component={SearchProductComponent}></ProtectedRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </Router>
    )
}

export default AppRoutes;

// routing
// react router dom
// browserrouter switch route
// browserrouter is provider switch assure only one config is running
// route is config block which takes props like path, component,exact

// link is used to navigate from templates(view file) <Link to="path">
// props=> history match and location

