import React, { Component } from 'react';
import { httpClient } from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { withRouter } from 'react-router-dom';

const IMG_URL = process.env.REACT_APP_IMG_URL;
const BASE_URL = process.env.REACT_APP_BASE_URL;

const defaultForm = {
    name: '',
    category: '',
    brand: '',
    description: '',
    price: '',
    color: '',
    tags: '',
    image: '',
    manuDate: '',
    expiryDate: '',
    discountedItem: false,
    discountType: '',
    discount: '',
    warrantyItem: false,
    warrantyPeroid: ''
}

class ProductFormComponent extends Component {
    uploadArray = [];
    constructor() {
        super();
        this.state = {
            title: '',
            data: { ...defaultForm },
            error: { ...defaultForm },
            isSubmitting: false,
            isValidForm: false
        }
    }
    componentDidMount() {
        console.log('this.props at productForm >>', this.props);
        if (this.props.title) {
            this.setState({
                title: this.props.title
            })
        }
        if (this.props.productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.productData,
                    discountedItem: this.props.productData.discount
                        && this.props.productData.discount.discountedItem
                        ? true
                        : false,
                    discountType: this.props.productData.discount
                        && this.props.productData.discount.discountType
                        ? this.props.productData.discount.discountType
                        : '',
                    discount: this.props.productData.discount
                        && this.props.productData.discount.discount
                        ? this.props.productData.discount.discount
                        : ''

                }
            })
        }

    }

    handleChange = e => {
        let { name, value, type, checked, files } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (type === 'file') {
            this.uploadArray = files;
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Category is required'
                break;

            default:
                break;
        }

        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            this.checkFormValidity();
        })
    }

    checkFormValidity() {
        const { error } = this.state;
        let errors = Object
            .values(error)
            .filter(err => err);

        this.setState({
            isValidForm: errors.length === 0,
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        if (this.state.data._id) {
            this.update();
        }
        else {
            this.add();
        }
    }

    add() {
        let url = BASE_URL + '/product?token=' + localStorage.getItem('token')
        httpClient.upload("POST", url, this.state.data, this.uploadArray)
            .then(response => {
                notification.showInfo("Product added successfully");
                this.props.history.push('/view-products');
            })
            .catch(err => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }
    update() {
        let url = BASE_URL + '/product/' + this.state.data._id + '?token=' + localStorage.getItem('token');
        httpClient.upload("PUT", url, this.state.data, this.uploadArray)
            .then(response => {
                notification.showInfo("Product Updated successfully");
                this.props.history.push('/view-products');
            })
            .catch(err => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let preImage = this.state.data.images && this.state.data.images.length
            ? <img src={`${IMG_URL}${this.state.data.images[0]}`} alt="old_image.png" width="400px"></img>
            : '';
        let discountContent = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <input className="form-control" type="text" placeholder="Discount Type" value={this.state.data.discountType} name="discountType" onChange={this.handleChange}></input>
                <label>Discount</label>
                <input className="form-control" type="text" placeholder="Discount" name="discount" value={this.state.data.discount} onChange={this.handleChange}></input>
            </>
            : '';

        let warrantyContent = this.state.data.warrantyItem
            ? <>
                <label>Warranty Period</label>
                <input className="form-control" type="text" placeholder="Warranty Period" name="warrantyPeroid" value={this.state.data.warrantyPeroid} onChange={this.handleChange}></input>
            </>
            : '';

        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} type="submit" className="btn btn-primary">submit</button>
        return (
            <>
                <h2>{this.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input className="form-control" type="text" placeholder="Name" value={this.state.data.name} name="name" onChange={this.handleChange}></input>
                    <label>Description</label>
                    <input className="form-control" type="text" placeholder="Description" value={this.state.data.description} name="description" onChange={this.handleChange}></input>
                    <label>Category</label>
                    <input className="form-control" type="text" placeholder="Category" value={this.state.data.category} name="category" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.category}</p>
                    <label>Brand</label>
                    <input className="form-control" type="text" placeholder="Brand" value={this.state.data.brand} name="brand" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input className="form-control" type="text" placeholder="Color" value={this.state.data.color} name="color" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input className="form-control" type="text" placeholder="Price" value={this.state.data.price} name="price" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input className="form-control" type="text" placeholder="Tags" value={this.state.data.tags} name="tags" onChange={this.handleChange}></input>
                    <label>Manu Date</label>
                    <input className="form-control" type="date" name="manuDate" value={this.state.data.manuDate} onChange={this.handleChange}></input>
                    <label>Expiry Date</label>
                    <input className="form-control" type="date" value={this.state.data.expiryDate} name="expiryDate" onChange={this.handleChange}></input>
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    <br></br>
                    {discountContent}
                    <input type="checkbox" name="warrantyItem" checked={this.state.data.warrantyItem} onChange={this.handleChange}></input>
                    <label>Warranty Item</label>
                    <br></br>
                    {warrantyContent}
                    {preImage}
                    <br />
                    <input type="file" className="form control" name="image" onChange={this.handleChange}></input>
                    <br></br>
                    {btn}
                </form>
            </>
        )
    }
}


export default withRouter(ProductFormComponent);
