import React, { Component } from 'react';
import ProductForm from './../productForm/productForm';
import { httpClient } from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { Loader } from './../../common/loader/loader.component';

export default class EditProductComponent extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            product: ''
        };
    }

    componentDidMount() {
        this.productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        });
        httpClient.get(`/product/${this.productId}`, {}, true)
            .then((response) => {
                console.log('response >>', response);
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }
    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm title="Edit Product" productData={this.state.product}></ProductForm>
        return (
            content
        )
    }
}

