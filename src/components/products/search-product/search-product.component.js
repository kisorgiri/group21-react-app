import React, { Component } from 'react'
import { httpClient } from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import ViewProductComponent from '../view-products/view-product.component';
const defaultForm = {
    category: '',
    name: '',
    minPrice: '',
    maxPrice: '',
    brand: '',
    fromDate: '',
    toDate: '',
    tags: ''
}
export class SearchProductComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: { ...defaultForm },
            error: { ...defaultForm },
            categories: [],
            allProducts: [],
            namesArray: [],
            isSubmitting: false,
            searchResults: [],
            isLoading: false
        }
    }

    componentDidMount() {
        this.setState({
            isLoading: true,
        })
        httpClient.post('/product/search', {}, {})
            .then(response => {
                let categories = [];
                response.data.forEach((item) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                });
                this.setState({
                    categories,
                    allProducts: response.data
                });
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    handleChange = e => {
        let { name, type, value, checked } = e.target;
        if (name === 'category') {
            this.filterNames(value);
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    filterNames(category) {
        let names = this.state.allProducts.filter(item => item.category === category)
        this.setState({
            namesArray: names
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.search(this.state.data);
    }

    resetSearch = () => {
        this.setState({
            data: { ...defaultForm },
            searchResults: []
        });
    }

    search(data) {
        if (!data.multipleDateRange) {
            data.toDate = null;
        }
        if (!data.toDate) {
            data.toDate = data.fromDate
        }

        this.setState({
            isSubmitting: true
        })
        httpClient.post('/product/search', data)
            .then(response => {
                if (!response.data.length) {
                    notification.showInfo("No any product matched your search query");
                }
                this.setState({
                    searchResults: response.data
                });

            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true} >searching...</button>
            : <button className="btn btn-primary" type="submit" >search</button>
        let categoryContent = this.state.categories.map((item, i) => (
            <option key={i} value={item}>{item}</option>
        ));
        let nameContent = this.state.namesArray.map(item => (
            <option key={item._id} value={item.name}>{item.name}</option>
        ));
        let toDateContent = this.state.data.multipleDateRange
            ? <>
                <label>To Date</label>
                <input type="date" name="toDate" className="form-control" onChange={this.handleChange}></input>
            </>
            : '';

        let name = this.state.namesArray.length
            ? <>
                <label>Name</label>
                <select name="name" className="form-control" onChange={this.handleChange}>
                    <option disabled={true}>(Select Name)</option>
                    {nameContent}
                </select>
            </>
            : ''

        let mainContent = this.state.searchResults.length
            ?
            <>
                <button className="btn btn-success" onClick={this.resetSearch}>search again</button>
                <ViewProductComponent productData={this.state.searchResults}></ViewProductComponent>
            </>
            : <>
                <h2>Search Product</h2>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Category</label>
                    {/* <input type="text" name="category" placeholder="Category" className="form-control" onChange={this.handleChange}></input> */}
                    <select className="form-control" name="category" onChange={this.handleChange}>
                        <option disabled={true} value="">(Select Category)</option>
                        {categoryContent}
                    </select>
                    {name}
                    <label>Brand</label>
                    <input type="text" name="brand" placeholder="Brand" className="form-control" onChange={this.handleChange}></input>
                    <label>Min Price</label>
                    <input type="number" name="minPrice" placeholder="0.00" className="form-control" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input type="text" name="maxPrice" placeholder="0.00" className="form-control" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input type="text" name="tags" placeholder="Tags" className="form-control" onChange={this.handleChange}></input>
                    <label>Date</label>
                    <input type="date" name="fromDate" className="form-control" onChange={this.handleChange}></input>
                    <input type='checkbox' name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br />
                    {toDateContent}
                    <br />
                    {btn}

                </form>
            </>
        return (
            mainContent
        )
    }
}
