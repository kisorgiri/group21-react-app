import React, { Component } from 'react'
import { httpClient } from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { Loader } from './../../common/loader/loader.component';
import util from './../../../utils/util';
const IMG_URL = process.env.REACT_APP_IMG_URL;

export default class ViewProductComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoading: false
        }
    }
    componentDidMount() {
        if (this.props.productData) {
            this.setState({
                products: this.props.productData
            })
        } else {
            this.setState({
                isLoading: true
            })
            httpClient.get('/product', {}, true)
                .then((response) => {
                    this.setState({
                        products: response.data
                    })
                })
                .catch(err => {
                    notification.handleError(err);
                })
                .finally(() => {
                    this.setState({
                        isLoading: false
                    })
                })
        }
    }

    editProduct = (id) => {
        console.log('id >>', id);
        this.props.history.push(`/edit-product/${id}`)
    }
    deleteProduct = (id, index) => {
        console.log('id >>', id + 'index>>' + index);
        // eslint-disable-next-line no-restricted-globals
        let confirmation = confirm("Are you sure to remove?");
        if (confirmation) {
            httpClient.remove(`/product/${id}`, true)
                .then(response => {
                    notification.showInfo("Product Deleted");
                    const { products } = this.state;
                    products.splice(index, 1);
                    this.setState({
                        products
                    });
                })
                .catch(err => {
                    notification.handleError(err);
                })
        }
    }

    render() {
        let tableContent = (this.state.products || []).map((item, index) => (
            <tr key={item._id}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.category}</td>
                <td>{item.price}</td>
                <td>{util.formatDate(item.createdAt)}</td>
                <td>{util.formatTime(item.createdAt)}</td>
                <td>
                    <img src={`${IMG_URL}${item.images[0]}`} alt="product_image.jpg" width="200px"></img>
                </td>
                <td>
                    <button onClick={() => this.editProduct(item._id)} className="btn btn-info">edit</button>
                    <button onClick={() => this.deleteProduct(item._id, index)} className="btn btn-danger">delete</button>
                </td>
            </tr>
        ));

        let mainContent = this.state.isLoading
            ? <Loader></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Created Time</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>

                </thead>
                <tbody>
                    {tableContent}
                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {mainContent}
            </>
        )
    }
}
