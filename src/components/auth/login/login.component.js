import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import notify from './../../../utils/notification';
import { httpClient } from './../../../utils/httpClient';

export class LoginComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: {

            },
            error: {

            },
            isSubmitting: false,
            isValidForm: false,
            remember_me: false,
        }; // it holds login component data
    }

    componentDidMount() {
        if (localStorage.getItem('remember_me')) {
            this.props.history.push('/dashboard/ldaskjf');
        }

        // this.interval = setInterval(() => {
        //     const { chapter } = this.state;
        //     let newChapter = chapter + 1;
        //     this.setState({
        //         chapter: newChapter
        //     })
        // }, 1000)

    }


    handleChange(e) {
        let { type, name, value, checked } = e.target;
        if (type === 'checkbox') {
            value = checked;
            this.rememberMe(value);
        }

        // this.setState({
        //     [name]: value
        // });
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }

        }), () => {
            // callback block
            this.validateForm(name);
        })

        // setState is used to change current state
        // anytime we call setState our render block is re called
        // this.state.username = e.target.value
    }

    rememberMe(val) {

        // webstorage
        localStorage.setItem('remember_me', val);
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state[fieldName]
                    ? ''
                    : 'Username is required'
                break;
            case 'password':
                errMsg = this.state[fieldName]
                    ? this.state[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'Password is required'
                break;

            default:
                break;
        }

        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }))

    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient.post('/auth/login', this.state.data)
            .then(response => {
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user', JSON.stringify(response.data.user))
                this.props.history.push('/dashboard'); //TODO appropriate way

            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">Logging in...</button>
            : <button className="btn btn-primary" type="submit">Login</button>
        // class based component must have render method
        // all component logic should be inside render method
        // render must have return block which must return single html node
        return <div>
            <h2>Login</h2>
            <p>Please Login to start your session</p>
            <form className="form-group" onSubmit={this.handleSubmit}>
                <label>Username</label>
                <input className="form-control" type="text" placeholder="Username" name="username" onChange={this.handleChange.bind(this)} ></input>
                <p className="danger">{this.state.usernameErr}</p>
                <label>Password</label>
                <input className="form-control" type="text" placeholder="Password" name="password" onChange={this.handleChange.bind(this)} ></input>
                <p className="danger">{this.state.passwordErr}</p>
                <input type="checkbox" name="remember_me" onChange={this.handleChange.bind(this)}></input>
                <label>Remeber Me</label>
                <br></br>
                {btn}
                <p>Don't have an account?
                    <Link to="/register">register here</Link>
                </p>
                <p>
                    <Link to="/forgot-password">forgot password?</Link>
                </p>
            </form>
            <p>learning chapter {this.state.chapter}</p>
        </div>;
    }
}