import React from 'react';

export class DashboardComponent extends React.Component {

    constructor() {
        super();
        this.state = {}
        console.log('here at dashboard');
    }

    componentDidMount() {
        console.log('props >>', this.props);
    }

    render() {
        return (
            <div>
                <p>Welcome to Group21 Web Store</p>
                <p>Please use side navigation menu or contact system administrator for support</p>
            </div>
        )
    }
}