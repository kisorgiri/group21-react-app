import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './header.component.css';
const logout = (props) => {
    // * TODO navigate through logout method */ }
    localStorage.clear();
    props.history.push('/');
    // props(history) will not be here because headerComponent is not defined inside Route of react rotuer dom
}
const HeaderComponent = function (props) {
    let navBar = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/about">About</Link>
            </li>
            <li className="nav_item">
                <Link to="/contact">Contact</Link>
            </li>
            <li className="float-right m-r-10">
                {/* TODO navigate through logout method */}
                <button className="btn btn-info" onClick={() => logout(props)} >logout</button>
            </li>
        </ul>
        :
        <ul className="nav_list">
            <li className="nav_item">
                <Link to="/">Home</Link>

            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>

            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>
    return (
        navBar
    )
}

export const Header = withRouter(HeaderComponent);