import React from 'react';
import { Link } from 'react-router-dom';
import './sidebar.component.css';
export const Sidebar = (props) => {
    return (
        <>
            <Link to="/dashboard">Home</Link>
            <Link to="/add-product">Add Product</Link>
            <Link to="/view-products">View Products</Link>
            <Link to="/search-product">Search Product</Link>
        </>
    )
}