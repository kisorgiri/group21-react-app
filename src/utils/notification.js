import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}
function showInfo(msg) {
    toast.info(msg);
}
function showWarning(msg) {
    toast.warn(msg);
}

function handleError(error) {
    let errMsg = "something went wrong";
    if (error) {
        let err = error.response && error.response.data;
        // server validation message
        if (err) {
            errMsg = err.msg;
        }
    }
    // error can be anything
    // step1 check error
    // step2 parse appropriate error message
    // step 3 show in ui
    showError(errMsg);
}

function showError(msg) {
    toast.error(msg);
}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}